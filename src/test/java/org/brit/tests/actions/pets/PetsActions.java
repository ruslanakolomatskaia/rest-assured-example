package org.brit.tests.actions.pets;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.brit.tests.Authentication;
import org.brit.tests.classes.MessageResponse;
import org.brit.tests.classes.Pet;
import org.brit.tests.classes.StatusEnum;

import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Created by sbryt on 9/9/2016.
 */
public class PetsActions {
    public static String PET_ENDPOINT = "/pet";

    private RequestSpecification requestSpecification;

    public PetsActions() {
        RestAssured.baseURI = "https://petstore.swagger.io";
        RestAssured.basePath = "/v2";
        requestSpecification = new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .addHeader("api_key", Authentication.getApiKey())
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL).build();
    }

    public Pet addNewPet(Pet request) {
        return given(requestSpecification)
                .body(request)
                .post(PET_ENDPOINT).as(Pet.class);
    }

    public List<Pet> getPetsByStatus(StatusEnum status) {
        return given(requestSpecification)
                .queryParam("status", StatusEnum.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then().log().all()
                .extract().body()
                .jsonPath().getList("", Pet.class);

    }

    public void deletePet(String petId) {
        given(requestSpecification)
                .pathParam("petId", petId)
                .delete(PET_ENDPOINT + "/{petId}");
    }

    public void deletePet(Pet pet) {
       deletePet(pet.getId().toString());
    }


    public boolean isPetExists(Pet pet) {
        return isPetExists(pet.getId().toString());
    }

    public boolean isPetExists(String petId) {
        return !given(requestSpecification)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .extract().body().jsonPath().getObject("", MessageResponse.class)
                .getMessage().equals("Pet not found");
    }

    public Pet updatePet(Pet pet) {
        return given(requestSpecification)
                .body(pet)
                .put(PET_ENDPOINT)
                .as(Pet.class);
    }
}
