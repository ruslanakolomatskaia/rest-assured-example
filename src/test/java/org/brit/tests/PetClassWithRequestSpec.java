package org.brit.tests;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;
import org.brit.tests.classes.MessageResponse;
import org.brit.tests.classes.StatusEnum;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.ThreadLocalRandom;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by sbryt on 8/26/2016.
 */
public class PetClassWithRequestSpec extends BaseTestClass {
    private Integer petId = ThreadLocalRandom.current().nextInt(100000, 999999);
    private String petName = "MyLittlePet_" + RandomStringUtils.randomAlphanumeric(5);
    private String newPetName = "NewName_" + RandomStringUtils.randomAlphanumeric(5);
    private StatusEnum status = StatusEnum.pending;
    private StatusEnum newStatus = StatusEnum.available;


    RequestSpecification requestSpecification = new RequestSpecBuilder()
            .setContentType(ContentType.JSON)
            .addHeader("api_key", Authentication.getApiKey())
            .log(LogDetail.ALL).build();

    @BeforeClass
    public void beforeClass() {
        addNewPet();
    }

    @AfterClass
    public void afterClass() {
        deletePetById(petId);
    }

    @Test
    public void getPetsByStatus() {
        given(requestSpecification)
                .queryParam("status", StatusEnum.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then().extract().body().jsonPath().prettyPrint();
    }

    @Test
    public void getPetByIdAndDoCheck() {
        given(requestSpecification)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .statusCode(200)
                .assertThat()
                .body("name", equalTo(petName),
                        "id", equalTo(petId),
                        "status", equalTo(status.toString()));
    }

    @Test(dependsOnMethods = "getPetByIdAndDoCheck")
    public void updateExistingPet() {
        given(requestSpecification)
                .body("{\n" +
                        "  \"id\": " + petId + ",\n" +
                        "  \"name\": \"" + newPetName + "\",\n" +
                        "  \"photoUrls\": [\n" +
                        "    \"string\"\n" +
                        "  ],\n" +
                        "  \"tags\": [],\n" +
                        "  \"status\": \"" + newStatus.toString() + "\"\n" +
                        "}")
                .put(PET_ENDPOINT)
                .then().extract().body().jsonPath().prettyPrint();

        given(requestSpecification)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .body("name", equalTo(newPetName))
                .extract().body().jsonPath()
                .prettyPrint();
    }


    private void addNewPet() {
        given(requestSpecification)
                .body("{\n" +
                        "  \"id\": " + petId + ",\n" +
                        "  \"name\": \"" + petName + "\",\n" +
                        "  \"photoUrls\": [],\n" +
                        "  \"tags\": [],\n" +
                        "  \"status\": \"" + status.toString() + "\"\n" +
                        "}")
                .post(PET_ENDPOINT);

        given(requestSpecification)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .body("name", equalTo(petName))
                .extract().body().jsonPath()
                .prettyPrint();
    }

    private void deletePetById(Integer petId) {
        given(requestSpecification)
                .pathParam("petId", petId)
                .expect().statusCode(200)
                .when()
                .delete(PET_ENDPOINT + "/{petId}");

        Assert.assertEquals(
                given(requestSpecification)
                        .pathParam("petId", petId)
                        .get(PET_ENDPOINT + "/{petId}")
                        .then()
                        .extract().body().jsonPath().getObject("", MessageResponse.class)
                        .getMessage(), "Pet not found");
    }


}
